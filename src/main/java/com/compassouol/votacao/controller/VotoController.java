package com.compassouol.votacao.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.compassouol.votacao.model.TotalVotos;
import com.compassouol.votacao.model.Voto;
import com.compassouol.votacao.service.VotoService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/votos")
public class VotoController {

	@Autowired
	private VotoService votoService;

	@GetMapping("/{id}")
	@ApiOperation(value = "Este método retorna os dados do voto identificado por id.")
	Voto readVoto(@PathVariable String id) {
		return votoService.readVoto(id);
	}

	@GetMapping
	@ApiOperation(value = "Este método retorna todos os votos cadastrados.")
	List<Voto> findAll() {
		return votoService.findAll();
	}
	
	@GetMapping("/search")
	@ApiOperation(value = "Este método retorna por sessão, o total de votos 'S' e o total de votos 'N'.")
	List<TotalVotos> findTotalVotos() {
		return votoService.findTotalVotos();
	}
}
