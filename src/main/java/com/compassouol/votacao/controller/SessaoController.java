package com.compassouol.votacao.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.compassouol.votacao.model.Sessao;
import com.compassouol.votacao.model.Voto;
import com.compassouol.votacao.model.VotoRequest;
import com.compassouol.votacao.service.SessaoService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/sessoes")
public class SessaoController {

	@Autowired
	private SessaoService sessaoService;

	@GetMapping("/{id}")
	@ApiOperation(value = "Este método retorna a sessão identificada por id.")
	Sessao readSessao(@PathVariable String id) {
		return sessaoService.readSessao(id);
	}

	@DeleteMapping("/{id}")
	@ApiOperation(value = "Este método exclui a sessão identificada por id.")
	void deleteSessao(@PathVariable String id) {
		sessaoService.deleteSessao(id);
	}

	@PostMapping("/{id}/votos")
	@ApiOperation(value = "Este método inclui um novo voto na sessão identificada por id.")
	Voto createVoto(@PathVariable String id, @Valid @RequestBody VotoRequest votoRequest) {
		Voto voto = new Voto(votoRequest.getCpf(), votoRequest.getVoto());
		return sessaoService.createVoto(id, voto);
	}
	
	@GetMapping
	@ApiOperation(value = "Este método retorna todas as sessões cadastradas.")
	List<Sessao> findAll() {
		return sessaoService.findAll();
	}
	
	@GetMapping("/search")
	@ApiOperation(value = "Este método retorna as sessões abertas (open=1) ou finalizadas (open=0 - default).")
	List<Sessao> findAllApplyingFilter(@RequestParam(value="open", defaultValue="0") String open) {
		return sessaoService.findAllApplyingFilter(open);
	}
	
}
