package com.compassouol.votacao.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.compassouol.votacao.model.Pauta;
import com.compassouol.votacao.model.PautaRequest;
import com.compassouol.votacao.model.Sessao;
import com.compassouol.votacao.model.SessaoRequest;
import com.compassouol.votacao.service.PautaService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/pautas")
public class PautaController {
	
	@Autowired
	private PautaService pautaService;
	
	@PostMapping
	@ApiOperation(value = "Este método inclui uma nova pauta com id gerado automaticamente.")
	Pauta createPauta(@Valid @RequestBody PautaRequest pautaRequest) {
		Pauta pauta = new Pauta(pautaRequest.getDescricao());
		return pautaService.createPauta(pauta);
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Este método retorna a pauta identificada por id.")
	Pauta readPauta(@PathVariable String id) {
		return pautaService.readPauta(id);
	}
	
	@PutMapping("/{id}")
	@ApiOperation(value = "Este método atualiza a descrição da pauta identificada por id.")
	Pauta updatePauta(@Valid @RequestBody Pauta pauta, @PathVariable String id) {
		return pautaService.updatePauta(pauta, id);
	}

	@DeleteMapping("/{id}")
	@ApiOperation(value = "Este método exclui a pauta identificada por id.")
	void deletePauta(@PathVariable String id) {
		pautaService.deletePauta(id);
	}

	@PostMapping("/{id}/sessoes")
	@ApiOperation(value = "Este método inclui uma nova sessão para pauta identificada por id.")
	Sessao createSessao(@PathVariable String id, @Valid @RequestBody SessaoRequest sessaoRequest) {
		Sessao sessao = new Sessao(sessaoRequest.getInicio(), sessaoRequest.getTermino());
		return pautaService.createSessao(id, sessao);
	}
	
	@GetMapping
	@ApiOperation(value = "Este método retorna todas as pautas cadastradas.")
	List<Pauta> findAll() {
		return pautaService.findAll();
	}
	
}
