package com.compassouol.votacao.model;

import javax.validation.constraints.NotBlank;

import org.springframework.data.annotation.Id;

public class Pauta {

	@Id
	private String id;

	@NotBlank
	private String descricao;
		
	public Pauta() {}

	public Pauta(String descricao) {
		this.descricao = descricao;
	} 

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public String toString() {
		return "Pauta {" + "id=" + this.id + ", descricao='" + this.descricao + "'}";
	}
	
}
