package com.compassouol.votacao.model;

import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

public class Sessao {

	@Id
	private String id;
	
	@NotNull
	private LocalDateTime inicio;
	
	private LocalDateTime termino;
	
	@DBRef
	private Pauta pauta;
	
	public Sessao(LocalDateTime inicio, LocalDateTime termino) {
		this.inicio = inicio;
		this.termino = termino;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public LocalDateTime getInicio() {
		return inicio;
	}

	public void setInicio(LocalDateTime inicio) {
		this.inicio = inicio;
	}

	public LocalDateTime getTermino() {
		return termino;
	}

	public void setTermino(LocalDateTime termino) {
		this.termino = termino;
	}
	
	public Pauta getPauta() {
		return pauta;
	}

	public void setPauta(Pauta pauta) {
		this.pauta = pauta;
	}

	@Override
	public String toString() {
		return "Sessao {" + "id=" + this.id + ", inicio='" + this.inicio + ", termino='" + this.termino + ", pauta.id ='" + this.pauta.getId() + "'}";
	}
	
	
}
