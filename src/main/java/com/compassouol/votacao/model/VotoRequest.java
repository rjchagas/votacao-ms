package com.compassouol.votacao.model;

import javax.validation.constraints.NotBlank;

public class VotoRequest {
	
	@NotBlank
	private String cpf;
	
	@NotBlank
	private String voto;

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getVoto() {
		return voto;
	}

	public void setVoto(String voto) {
		this.voto = voto;
	}

}
