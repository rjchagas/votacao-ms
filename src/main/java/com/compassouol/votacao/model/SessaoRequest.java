package com.compassouol.votacao.model;

import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;

public class SessaoRequest {

	@NotNull
	private LocalDateTime inicio;
	
	private LocalDateTime termino;

	public LocalDateTime getInicio() {
		return inicio;
	}

	public void setInicio(LocalDateTime inicio) {
		this.inicio = inicio;
	}

	public LocalDateTime getTermino() {
		return termino;
	}

	public void setTermino(LocalDateTime termino) {
		this.termino = termino;
	}
}
