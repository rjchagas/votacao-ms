package com.compassouol.votacao.model;

import javax.validation.constraints.NotBlank;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

public class Voto {

	@Id
	private String id;
	
	@NotBlank
	private String cpf;
	
	@NotBlank
	private String voto;
	
	@DBRef
	private Sessao sessao;
	
	public Voto(String cpf, String voto) {
		this.cpf = cpf;
		this.voto = voto;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getVoto() {
		return voto;
	}

	public void setVoto(String voto) {
		this.voto = voto;
	}
	
	public Sessao getSessao() {
		return sessao;
	}

	public void setSessao(Sessao sessao) {
		this.sessao = sessao;
	}

	@Override
	public String toString() {
		return "Voto {" + "id=" + this.id + ", cpf='" + this.cpf + ", voto='" + this.voto + ", sessao.id ='" + this.sessao.getId() + "'}";
	}
	
}
