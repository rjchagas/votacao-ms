package com.compassouol.votacao.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.compassouol.votacao.model.TotalVotos;
import com.compassouol.votacao.model.Voto;

public interface VotoRepository extends MongoRepository<Voto, String> {

	public List<TotalVotos> findTotalVotos();
	
}
