package com.compassouol.votacao.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.compassouol.votacao.model.Pauta;

public interface PautaRepository extends MongoRepository<Pauta, String> {

}
