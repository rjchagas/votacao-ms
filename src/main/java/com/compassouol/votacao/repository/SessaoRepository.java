package com.compassouol.votacao.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.compassouol.votacao.model.Sessao;;

public interface SessaoRepository extends MongoRepository<Sessao, String>  {
	
	List<Sessao> findAllApplyingFilter(String open);
	
}
