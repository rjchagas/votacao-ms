package com.compassouol.votacao.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;
import org.springframework.data.mongodb.core.aggregation.SortOperation;
import org.springframework.stereotype.Component;

import com.compassouol.votacao.model.TotalVotos;

@Component
public class VotoRepositoryImpl {
	
	@Autowired
	private MongoTemplate mongoTemplate;

	public List<TotalVotos> findTotalVotos() {
		
		GroupOperation group = Aggregation.group("voto", "sessao").count().as("total");
		ProjectionOperation projection = Aggregation.project("sessao", "voto", "total").andExclude("_id");
        SortOperation sort = Aggregation.sort(Sort.by(Sort.Direction.ASC, "total"));

        Aggregation aggregation = Aggregation.newAggregation(group, projection, sort);

        AggregationResults<TotalVotos> result = mongoTemplate.aggregate(aggregation, "voto", TotalVotos.class);
      
        return result.getMappedResults();
	}
}
