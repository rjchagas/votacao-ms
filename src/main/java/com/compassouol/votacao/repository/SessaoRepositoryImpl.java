package com.compassouol.votacao.repository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import com.compassouol.votacao.model.Sessao;

@Component
public class SessaoRepositoryImpl {

	@Autowired
	private MongoTemplate mongoTemplate;

	public List<Sessao> findAllApplyingFilter(String open) {
		
		final List<Criteria> criterias = new ArrayList<>();
		
		if (open.equalsIgnoreCase("0")) {
			Criteria c1 = Criteria.where("termino").lt(LocalDateTime.now());
			criterias.add(c1);
		} 
		if (open.equalsIgnoreCase("1")) {
			Criteria c1 = Criteria.where("inicio").lte(LocalDateTime.now());
			Criteria c2 = Criteria.where("termino").gte(LocalDateTime.now());
			criterias.add(c1.andOperator(c2));
		} 
			
		final Query query = new Query();
		criterias.stream().forEach(c -> query.addCriteria(c));
	
		return mongoTemplate.find(query, Sessao.class);
	}

}
