package com.compassouol.votacao.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.compassouol.votacao.exception.CpfValidationJsonException;
import com.compassouol.votacao.exception.EndedSessionException;
import com.compassouol.votacao.exception.InvalidCpfException;
import com.compassouol.votacao.exception.ItemNotFoundException;
import com.compassouol.votacao.exception.NotAccessedCpfValidationException;
import com.compassouol.votacao.model.Sessao;
import com.compassouol.votacao.model.Voto;
import com.compassouol.votacao.repository.PautaRepository;
import com.compassouol.votacao.repository.SessaoRepository;
import com.compassouol.votacao.repository.VotoRepository;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class SessaoServiceImpl implements SessaoService {

	@Autowired
	private PautaRepository pautaRepository;

	@Autowired
	private SessaoRepository sessaoRepository;

	@Autowired
	private VotoRepository votoRepository;
	
	public Sessao readSessao(String id) {
		return sessaoRepository.findById(id)
				.orElseThrow(() -> new ItemNotFoundException(id));
	}
	
	public void deleteSessao(String id) {
		if (sessaoRepository.existsById(id) ) {
			sessaoRepository.deleteById(id);
		} else throw new ItemNotFoundException(id);
	}
	
	public Voto createVoto(String id, Voto voto) {
		Sessao sessao = sessaoRepository.findById(id)
				.orElseThrow(() -> new ItemNotFoundException(id));
		
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = "https://user-info.herokuapp.com/users/";
		ResponseEntity<String> response = restTemplate.getForEntity(fooResourceUrl + voto.getCpf(), String.class);
		if (response.getStatusCode() != HttpStatus.OK) {
			throw new NotAccessedCpfValidationException();
		}
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			JsonNode root = mapper.readTree(response.getBody());
			JsonNode status = root.path("status");
			if (status.asText().equals("UNABLE_TO_VOTE")) {
				throw new InvalidCpfException();
			}
		} catch (InvalidCpfException e) {
			throw new InvalidCpfException();
		} catch (Exception e) {
			throw new CpfValidationJsonException();
		}
			
		if (sessao.getTermino().isBefore(LocalDateTime.now())) 
			throw new EndedSessionException(sessao.getId());
		voto.setSessao(sessao);
		return votoRepository.save(voto);		
	}

	public List<Sessao> findAll() {
		return sessaoRepository.findAll();
	}
	
	public List<Sessao> findAllApplyingFilter(String open) {
		return sessaoRepository.findAllApplyingFilter(open);
	}
	
}
