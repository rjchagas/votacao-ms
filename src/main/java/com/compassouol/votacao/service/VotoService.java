package com.compassouol.votacao.service;

import java.util.List;

import com.compassouol.votacao.model.TotalVotos;
import com.compassouol.votacao.model.Voto;

public interface VotoService {
	
	Voto readVoto(String id);
		
	List<Voto> findAll();
	
	List<TotalVotos> findTotalVotos();

}

