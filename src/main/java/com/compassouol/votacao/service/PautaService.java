package com.compassouol.votacao.service;

import java.util.List;

import com.compassouol.votacao.model.Pauta;
import com.compassouol.votacao.model.Sessao;

public interface PautaService {

	Pauta createPauta(Pauta pauta);
	
	Pauta readPauta(String id);
	
	Pauta updatePauta(Pauta pauta, String id);
	
	void deletePauta(String id);
	
	Sessao createSessao(String id, Sessao sessao);

	List<Pauta> findAll();

}
