package com.compassouol.votacao.service;

import java.util.List;

import com.compassouol.votacao.model.Sessao;
import com.compassouol.votacao.model.Voto;

public interface SessaoService {
	
	Sessao readSessao(String id);
		
	void deleteSessao(String id);
	
	Voto createVoto(String id, Voto voto);

	List<Sessao> findAll();
	
	List<Sessao> findAllApplyingFilter(String open);

}
