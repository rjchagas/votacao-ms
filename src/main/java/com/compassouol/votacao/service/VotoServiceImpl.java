package com.compassouol.votacao.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compassouol.votacao.exception.ItemNotFoundException;
import com.compassouol.votacao.model.TotalVotos;
import com.compassouol.votacao.model.Voto;
import com.compassouol.votacao.repository.SessaoRepository;
import com.compassouol.votacao.repository.VotoRepository;

@Component
public class VotoServiceImpl implements VotoService {

	@Autowired
	private SessaoRepository sessaoRepository;
	
	@Autowired
	private VotoRepository votoRepository;
	
	public Voto readVoto(String id) {
		return votoRepository.findById(id)
				.orElseThrow(() -> new ItemNotFoundException(id));
	}

	public List<Voto> findAll() {
		return votoRepository.findAll();
	}
	
	public List<TotalVotos> findTotalVotos() {
		return votoRepository.findTotalVotos();
	}
	
}
