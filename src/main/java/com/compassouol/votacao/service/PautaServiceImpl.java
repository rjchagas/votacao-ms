package com.compassouol.votacao.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compassouol.votacao.exception.InvalidDateException;
import com.compassouol.votacao.exception.ItemNotFoundException;
import com.compassouol.votacao.model.Pauta;
import com.compassouol.votacao.model.Sessao;
import com.compassouol.votacao.repository.PautaRepository;
import com.compassouol.votacao.repository.SessaoRepository;

@Component
public class PautaServiceImpl implements PautaService {

	@Autowired
	private PautaRepository pautaRepository;

	@Autowired
	private SessaoRepository sessaoRepository;
	
	public Pauta createPauta(Pauta pauta) {
		return pautaRepository.save(pauta);		
	}
	
	public Pauta readPauta(String id) {
		return pautaRepository.findById(id)
				.orElseThrow(() -> new ItemNotFoundException(id));
	}
	
	public Pauta updatePauta(Pauta pauta, String id) {
		return pautaRepository.findById(id)
				.map(updatedPauta -> {
					updatedPauta.setDescricao(pauta.getDescricao());
					return pautaRepository.save(updatedPauta);
				})
				.orElseThrow(() -> new ItemNotFoundException(id));
	}
	
	public void deletePauta(String id) {
		if (pautaRepository.existsById(id) ) {
			pautaRepository.deleteById(id);
		} else throw new ItemNotFoundException(id);
	}
	
	public Sessao createSessao(String id, Sessao sessao) {
		Pauta pauta = pautaRepository.findById(id)
				.orElseThrow(() -> new ItemNotFoundException(id));
		sessao.setPauta(pauta);
		if (sessao.getTermino() == null) {
			sessao.setTermino(sessao.getInicio().plusMinutes(1)); 	
		}
		if (sessao.getTermino().isBefore(sessao.getInicio()))
			throw new InvalidDateException();
		return sessaoRepository.save(sessao);		
	}

	public List<Pauta> findAll() {
		return pautaRepository.findAll();
	}
	
}
