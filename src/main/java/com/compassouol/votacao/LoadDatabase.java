package com.compassouol.votacao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.compassouol.votacao.model.Pauta;
import com.compassouol.votacao.repository.PautaRepository;
import com.compassouol.votacao.repository.SessaoRepository;
import com.compassouol.votacao.repository.VotoRepository;

@Configuration
class LoadDatabase {

	private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

	@Autowired
	private PautaRepository pautaRepository;
	
	@Autowired
	private SessaoRepository sessaoRepository;

	@Autowired
	private VotoRepository votoRepository;

	
	@Bean
	CommandLineRunner initDatabase() throws Exception {

		// todo: atualizar initDatabase
		
//		votoRepository.deleteAll();
//		sessaoRepository.deleteAll();
//		pautaRepository.deleteAll();

		return args -> {
			Pauta pauta = new Pauta(" pauta 1 ");
			//Sessao sessao = new Sessao(pauta, inicio, termino);
//			log.info("Preloading " + pautaRepository.save(new Pauta("Pauta 1")));
//			log.info("Preloading " + pautaRepository.save(new Pauta("Pauta 2")));
//			log.info("Preloading " + pautaRepository.save(new Pauta("Pauta 3")));
		};

	}

}