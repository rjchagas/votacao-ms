package com.compassouol.votacao.exception;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class CpfControllerAdvice {

	@ExceptionHandler(NotAccessedCpfValidationException.class)
	public ResponseEntity<Object> notAccessedCpfValidationHandler(NotAccessedCpfValidationException ex, WebRequest request) {

		Map<String, Object> body = new LinkedHashMap<>();
		body.put("status_code", HttpStatus.BAD_REQUEST.value());
		body.put("message", "Não obteve acesso ao serviço de validação de cpf.");

		return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST); 

	}
	
	@ExceptionHandler(InvalidCpfException.class)
	public ResponseEntity<Object> invalidCpfHandler(InvalidCpfException ex, WebRequest request) {

		Map<String, Object> body = new LinkedHashMap<>();
		body.put("status_code", HttpStatus.BAD_REQUEST.value());
		body.put("message", "Cpf inválido para votar. ");

		return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST); 

	}
	
	@ExceptionHandler(CpfValidationJsonException.class)
	public ResponseEntity<Object> cpfValidationJsonHandler(CpfValidationJsonException ex, WebRequest request) {

		Map<String, Object> body = new LinkedHashMap<>();
		body.put("status_code", HttpStatus.BAD_REQUEST.value());
		body.put("message", "Problema decodificação JSON da validação do cpf.");

		return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST); 

	}
	
}


