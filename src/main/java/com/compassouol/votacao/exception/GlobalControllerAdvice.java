package com.compassouol.votacao.exception;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import com.fasterxml.jackson.core.JsonParseException;

@ControllerAdvice
public class GlobalControllerAdvice {

	@ExceptionHandler(EndedSessionException.class)
	public ResponseEntity<Object> EndedSessionHandler(EndedSessionException ex, WebRequest request) {

		Map<String, Object> body = new LinkedHashMap<>();
		body.put("status_code", HttpStatus.BAD_REQUEST.value());
		body.put("message", "A sessão já foi finalizada.");

		return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST); 

	}
	
	@ExceptionHandler(InvalidDateException.class)
	public ResponseEntity<Object> InvalidDateHandler(InvalidDateException ex, WebRequest request) {

		Map<String, Object> body = new LinkedHashMap<>();
		body.put("status_code", HttpStatus.BAD_REQUEST.value());
		body.put("message", "A data de término deve ser posterior à data de início.");

		return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST); 

	}

	@ExceptionHandler(ItemNotFoundException.class)
	public ResponseEntity<Object> itemNotFoundHandler(ItemNotFoundException ex, WebRequest request) {

		Map<String, Object> body = new LinkedHashMap<>();
		body.put("status_code", HttpStatus.NOT_FOUND.value());
		body.put("message", "Id não encontrado.");

		return new ResponseEntity<>(body, HttpStatus.NOT_FOUND); 

	}
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<Object> MethodArgumentNotValidHandler(MethodArgumentNotValidException ex, WebRequest request) {

		Map<String, Object> body = new LinkedHashMap<>();
		body.put("status_code", HttpStatus.BAD_REQUEST.value());
		body.put("message", "Campos obrigatórios não preenchidos no JSON.");

		return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST); 

	}

	@ExceptionHandler(JsonParseException.class)
	public ResponseEntity<Object> JsonParseHandler(JsonParseException ex, WebRequest request) {

		Map<String, Object> body = new LinkedHashMap<>();
		body.put("status_code", HttpStatus.BAD_REQUEST.value());
		body.put("message", "O JSON não está no formato correto.");

		return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST); 

	}

}


