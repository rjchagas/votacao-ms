# Votação em Pautas #

## votacao-ms

Este microserviço contém as seguintes funcionalidades:

- Operações CRUD para determinada pauta: criar, visualizar, alterar e excluir.

- Cadastro de sessões em determinada pauta.

- Registro de votos em uma sessão que esteja em aberto.


## Copiar o repositório local

git clone https://bitbucket.org/rjchagas/votacao-ms.git

## Compilar e rodar o microserviço

./mvnw clean spring-boot:run

## Banco de dados

O banco de dados é o mondodb. 

Banco de dados: test_db
Usuário: abc
Senha: 123

### Configuração

As configurações estão no application.properties. Deve-se adicionar o seguinte usuário para validação.

```
db.createUser(
	{ 
		user : "abc", 
		pwd: "123",
		roles : [ { role : "readWrite", db : "test_db" } ] 
	}
)
```

## Acesso ao microserviço

O acesso é feito em localhost e porta 8080. Por exemplo, para listar todas as pautas cadastradas, utilize a requisição GET:

http://localhost:8080/pautas

Os endpoints estão disponíveis no seguinte endereço:

http://localhost:8080/swagger-ui/index.html


## A seguir estão 2 endpoints que podem ser acionados pelo terminal

- excluir pauta por id

curl --location --request DELETE 'http://localhost:9999/pautas/605e151dde0cda13e9fb770a'

- recuperar pauta por id

curl --location --request GET 'http://localhost:8080/pautas/606464edc7096052f943fd0b'


### Postman

O arquivo votacao-ms.postman_collection.json contém alguns exemplos das chamadas aos endpoints. Ele pode ser importado pelo aplicativo Postman.
